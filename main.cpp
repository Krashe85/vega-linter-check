#include <iostream>

namespace MyProject {
    class Entity {
    public:
        int* data( );

    private:
        int* _data;
        size_t _size;
    };
} // namespace MyProject

int main( )
{
    int* x;
    int k = 0;
    int a = -1;
    int c = 1;
    int d = 1;
    int e = 10;
    int i = k + a / (c + d) - e;
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
